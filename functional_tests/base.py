import pytest

__author__ = 'regu0004'


@pytest.mark.django_db
class FunctionalTest(object):
    def check_for_row_in_list_table(self, browser, row_text):
        table = browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        assert row_text in [row.text for row in rows]

    def get_item_input_box(self, browser):
        return browser.find_element_by_id('id_text')