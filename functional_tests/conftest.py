import pytest

from pytest_django.fixtures import live_server
from selenium import webdriver


def pytest_addoption(parser):
    parser.addoption("--url", default=None,
                     help="Specify url to server")


@pytest.fixture
def server_url(request):
    url = request.config.getoption("--url")
    if url:
        return url

    server = live_server(request)  # run dev server
    return server.url


@pytest.yield_fixture
def browser():
    browser = get_browser()
    browser.implicitly_wait(3)
    yield browser
    browser.quit()


def get_browser():
    return webdriver.PhantomJS()
