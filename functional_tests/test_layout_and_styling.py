from .base import FunctionalTest

__author__ = 'regu0004'


class LayoutAndStylingTest(FunctionalTest):
    def test_layout_and_styling(self, browser, server_url):
        # Edith goes to the home page
        browser.get(server_url)
        browser.set_window_size(1024, 768)

        # She notices the input box is nicely centered
        inputbox = self.get_item_input_box(browser)
        assert abs(
            inputbox.location['x'] + inputbox.size['width'] / 2 - 512) < 5

        # She starts a new list and sees the input is nicely centered there too
        inputbox.send_keys('testing\n')
        inputbox = self.get_item_input_box(browser)
        assert abs(
            inputbox.location['x'] + inputbox.size['width'] / 2 - 512) < 5
