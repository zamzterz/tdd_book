from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest

__author__ = 'regu0004'


class ItemValidationTest(FunctionalTest):
    def get_error_element(self, browser):
        return browser.find_element_by_css_selector('.has-error')

    def test_cannot_add_empty_list_items(self, browser, server_url):
        # Edith goes to the home page and accidentally tries to submit
        # an empty list item. She hits Enter on the empty input box
        browser.get(server_url)
        self.get_item_input_box(browser).send_keys(Keys.ENTER)

        # The home page refreshes, and there is an error message saying
        # that list items cannot be blank
        error = self.get_error_element(browser)
        assert error.text == "You can't have an empty list item"

        # She tries again with some text for the item, which now works
        self.get_item_input_box(browser).send_keys('Buy milk\n')
        self.check_for_row_in_list_table(browser, '1: Buy milk')

        # Perversely, she now decides to submit a second blank list item
        self.get_item_input_box(browser).send_keys(Keys.ENTER)
        self.check_for_row_in_list_table(browser, '1: Buy milk')
        error = self.get_error_element(browser)
        assert error.text == "You can't have an empty list item"

        # She receives a similar warning on the list page
        # And she can correct it by filling some text in
        self.get_item_input_box(browser).send_keys('Make tea\n')
        self.check_for_row_in_list_table(browser, '1: Buy milk')
        self.check_for_row_in_list_table(browser, '2: Make tea')

    def test_cannot_add_duplicate_items(self, browser, server_url):
        # Edith goes to the home page and starts a new list
        browser.get(server_url)
        self.get_item_input_box(browser).send_keys('Buy wellies\n')
        self.check_for_row_in_list_table(browser, '1: Buy wellies')

        # She accidentally tries to enter a duplicate item
        self.get_item_input_box(browser).send_keys('Buy wellies\n')

        # She sees a helpful error message
        self.check_for_row_in_list_table(browser, '1: Buy wellies')
        error = self.get_error_element(browser)
        assert error.text == "You've already got this in your list"

    def test_error_messages_are_cleared_on_input(self, browser, server_url):
        # Edith starts a new list in a way that causes a validation error
        browser.get(server_url)
        self.get_item_input_box(browser).send_keys(Keys.ENTER)
        error = self.get_error_element(browser)
        assert error.is_displayed()

        # She starts typing in the input box to clear the error
        self.get_item_input_box(browser).send_keys('a')
        error = self.get_error_element(browser)
        assert not error.is_displayed()

    def test_error_messages_are_cleared_on_click(self, browser, server_url):
        # Edith starts a new list in a way that causes a validation error
        browser.get(server_url)
        self.get_item_input_box(browser).send_keys(Keys.ENTER)
        error = self.get_error_element(browser)
        assert error.is_displayed()

        # She clicks in the input box to clear the error
        self.get_item_input_box(browser).click()
        error = self.get_error_element(browser)
        assert not error.is_displayed()
