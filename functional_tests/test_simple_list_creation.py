import re

from selenium.webdriver.common.keys import Keys
import time

from .base import FunctionalTest
from .conftest import get_browser

__author__ = 'regu0004'


class NewVisitorTest(FunctionalTest):
    def test_can_start_a_list_and_retrieve_it_later(self, browser, server_url):
        # Edith has heard about a cool new online to-do app.
        # She goes to check out its homepage
        browser.get(server_url)

        # She notices the page title and header mention to-do lists
        assert 'To-Do' in browser.title
        header_text = browser.find_element_by_tag_name('h1').text
        assert 'To-Do' in header_text

        # She is invited to enter a to-do item straight away
        inputbox = self.get_item_input_box(browser)
        assert inputbox.get_attribute('placeholder') == 'Enter a to-do item'

        # She types "Buy peacock feathers" into a text box (Edith's hobby
        # is tying fly-fishing lures)
        inputbox.send_keys('Buy peacock feathers')

        # When she hits enter, she is taken to a new URL, and the page lists
        # "1: Buy peacock feathers" as an item in a to-do list table
        inputbox.send_keys(Keys.ENTER)
        edith_list_url = browser.current_url
        assert re.search(r'/lists/.+', edith_list_url)
        self.check_for_row_in_list_table(browser, '1: Buy peacock feathers')

        # There is still a text box inviting her to add another item. She
        # enters "Use peacock feathers to make a fly" (Edith is very methodical)
        inputbox = self.get_item_input_box(browser)
        inputbox.send_keys('Use peacock feathers to make a fly')
        inputbox.send_keys(Keys.ENTER)

        # The page updates again, and now shows both items on her list
        self.check_for_row_in_list_table(browser, '1: Buy peacock feathers')
        self.check_for_row_in_list_table(browser,
                                         '2: Use peacock feathers to make a fly')

        # Now a new user, Francis, come along to the site

        # We use a new browser session to make sure that no information of
        # Edith's is coming through from cookies etc
        francis_browser = get_browser()

        # Francis visits the home page. There is no sign of Edith's  list
        francis_browser.get(server_url)
        page_text = francis_browser.find_element_by_tag_name('body').text
        assert 'Buy peacock feathers' not in page_text
        assert 'make a fly' not in page_text

        # Francis starts a new list by entering a new item. He
        # is less interesting than Edith...
        inputbox = self.get_item_input_box(francis_browser)
        inputbox.send_keys('Buy milk')
        inputbox.send_keys(Keys.ENTER)

        # Francis gets his own unique URL
        francis_list_url = francis_browser.current_url
        assert re.search(r'/lists/.+', francis_list_url)
        assert francis_list_url != edith_list_url

        # Again, there is no trace of Edith's list
        page_text = francis_browser.find_element_by_tag_name('body').text
        assert 'Buy peacock feathers' not in page_text
        assert 'Buy milk' in page_text

        francis_browser.quit()

        #  Satisfied, they both go back to sleep
