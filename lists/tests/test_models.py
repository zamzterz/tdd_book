from django.core.exceptions import ValidationError

import pytest

from lists.models import Item, List


class ItemModelTest(object):
    def test_default_text(self):
        item = Item()
        assert item.text == ''


class ListModelTest(object):
    @pytest.mark.django_db
    def test_get_absolute_url(self):
        list_ = List.objects.create()
        assert list_.get_absolute_url() == '/lists/{}/'.format(list_.id)


@pytest.mark.django_db
class ListAndItemModelTest(object):
    def test_saving_and_retrieving_items(self):
        list_ = List.objects.create()
        item = Item()
        item.list = list_
        item.save()

        assert item in list_.item_set.all()

    def test_cannot_save_empty_list_items(self):
        list_ = List.objects.create()
        item = Item(text='', list=list_)

        with pytest.raises(ValidationError):
            item.save()
            item.full_clean()

    def test_duplicate_items_are_invalid(self):
        list_ = List.objects.create()
        Item.objects.create(list=list_, text='bla')
        with pytest.raises(ValidationError):
            item = Item(list=list_, text='bla')
            item.full_clean()

    def test_CAN_save_same_item_to_different_lists(self):
        list1 = List.objects.create()
        list2 = List.objects.create()
        Item.objects.create(list=list1, text='bla')
        item = Item(list=list2, text='bla')
        item.full_clean()  # should not raise
