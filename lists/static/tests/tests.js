/*global $, test, equal */

QUnit.test("errors should be hidden on keypress", function () {
    var fixture = $("#qunit-fixture");
    $("input", fixture).trigger("keypress");
    equal($(".has-error", fixture).is(":visible"), false);
});

QUnit.test("errors not be hidden unless there is a keypress", function () {
    var fixture = $("#qunit-fixture");
    equal($(".has-error", fixture).is(":visible"), true);
});

QUnit.test("errors should be hidden on click", function () {
    // TODO this test should be working!
    var fixture = $("#qunit-fixture");
    $("input", fixture).trigger("click");
    equal($(".has-error", fixture).is(":visible"), false);
});